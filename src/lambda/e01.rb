#
# https://discord.com/channels/518658712081268738/650031651845308419/1143918694645379092
#

class Board
  SIZE = 8
end

ALGEBRAIC_ROW = ->(row) { (Board::SIZE - row).to_s }
ALGEBRAIC_COL = ->(col) { (col + 97).chr }

p Board::SIZE
#=> 8

p ALGEBRAIC_ROW.call(3)
#=> "5"

p ALGEBRAIC_COL.call(5)
#=> "f"


