#
# https://discord.com/channels/518658712081268738/650031651845308419/1143918694645379092
#

class Board
  SIZE = 8
end

class Foo
  FUNCTIONS = {
    algebraic_row: ->(row) { (Board::SIZE - row).to_s },
    algebraic_col: ->(col) { (col + 97).chr },
  }
end

p Foo::FUNCTIONS[:algebraic_row].call(3)
#=> "5"

p Foo::FUNCTIONS[:algebraic_col].call(5)
#=> "f"
