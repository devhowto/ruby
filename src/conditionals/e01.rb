#
# https://discord.com/channels/518658712081268738/650031651845308419/1144217109375762514
#

class Jedi
  attr_reader :name, :master

  def initialize(name, master = nil)
    @name = name
    @master = master
  end
end

yoda = Jedi.new('Yoda')
ahsoka = Jedi.new('Ahsoka Tano', yoda)

p ahsoka.name
#=> 'Ahsoka Tano'

p ahsoka.master.name
#=> 'Yoda'

p yoda&.master&.name
# ~ undefined method `name' for nil:NilClass (NoMethodError)
