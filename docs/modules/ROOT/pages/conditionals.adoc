= Conditionals
:page-subtitle: Ruby

== Safe Navigation Operator

Safe navigation operator is also known as optional chaining operator in some languages.

* link:https://en.wikipedia.org/wiki/Safe_navigation_operator[Safe Navigation Operator :: Wikipedia^].
* link:https://discord.com/channels/518658712081268738/650031651845308419/1144217109375762514[Ruby Discord thread on safe navigation operator^].

Consider this piece of code:

[source,ruby]
----
class Jedi
  attr_reader :name, :master

  def initialize(name, master = nil)
    @name = name
    @master = master
  end
end

yoda = Jedi.new('Yoda')
ahsoka = Jedi.new('Ahsoka Tano', yoda)
----

Note that Yoda doesn't have a master, while Ahsoka does.

[source,ruby]
----
p ahsoka.name
#=> 'Ahsoka Tano'

p ahsoka.master.name
#=> 'Yoda'

p yoda.master.name
# ~ undefined method `name' for nil:NilClass (NoMethodError)
----

Because Yoda doesn't have a master, we can't get the name of Yoda's master as it doesn't exist.

We could use the safe navigation operator to avoid an exception:

[source,ruby]
----
p yoda&.master&.name
#=> nil
----

We get back `nil` instead of an exception.
